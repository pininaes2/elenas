## Prerequisites

- [Git](http://git-scm.com/) Installed in your PC
- [Terraform](https://www.terraform.io/downloads.html) version >= 14.X installed in your PC
- [AWS](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) Credential to deploy Terraform
```aws
# An example related to how to use credential AWS
export AWS_ACCESS_KEY_ID="ASIARVVBUVEAWDZZJT5B"
export AWS_SECRET_ACCESS_KEY="Ff2bs8ELNqGlrJ4eJL7SLVXvDxza5jMV41enwScW"
```
- [TFVARS](https://www.terraform.io/docs/language/values/variables.html) Variables file configure. Check file in PATH terraform/terraform.tfvars

## Resources

| Name | Type |
|------|------|
| [aws_eip.eip_ec2_frondend](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_instance.ec2_backend](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_instance.ec2_frontend](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_key_pair.private_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |
| [aws_security_group.ec2_backend](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.ec2_frontend](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.ec2_backend_egress_all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ec2_backend_ingress_docker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ec2_backend_ingress_ssh](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ec2_frontend_egress_all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ec2_frontend_ingress_http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ec2_frontend_ingress_https](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ec2_frontend_ingress_ssh](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_subnet_ids.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet_ids) | data source |
| [template_file.userdata_backend](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |
| [template_file.userdata_frontend](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ec2_www_instance_type"></a> [ec2\_www\_instance\_type](#input\_ec2\_www\_instance\_type) | Instance type EC2 | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region location | `string` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID Account | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ec2_backend_public_ip"></a> [ec2\_backend\_public\_ip](#output\_ec2\_backend\_public\_ip) | n/a |
| <a name="output_ec2_frontend_elastic_ip"></a> [ec2\_frontend\_elastic\_ip](#output\_ec2\_frontend\_elastic\_ip) | n/a |
| <a name="output_ubuntu_ami_id"></a> [ubuntu\_ami\_id](#output\_ubuntu\_ami\_id) | n/a |

## Terraform Deploy
```terraform
# An example related to how to use Terraform
cd terraform/
terraform init
terraform validate
terraform apply
```

## Show APP
When terraform finishes doing the APPLY, let's see that it generates an output called ec2_frontend_elastic_ip with a public IP. We copy the public IP and wait 5 minutes for the instances to finish automatically. 
```output
# An example of the ec2_frontend_elastic_ip output 
Outputs:

ec2_backend_public_ip = "54.245.56.202"
ec2_frontend_elastic_ip = "35.84.251.32"
ubuntu_ami_id = "ami-080471172a731411b"
```
We open our preferred browser and paste the public IP of the frontend instances "http://35.84.251.32". 
## Terraform destroy
```terraform
# An example related to how to use Terraform
cd terraform/
terraform destroy
```
