output "ubuntu_ami_id" {
  value = data.aws_ami.ubuntu.id
}

output "ec2_frontend_elastic_ip" {
  value = aws_eip.eip_ec2_frondend.public_ip
}

output "ec2_backend_public_ip" {
  value = aws_instance.ec2_backend.public_ip
}