#!/bin/bash
apt update
apt dist-upgrade -y
apt install -y nginx joe tmux htop git npm
npm install --global yarn
systemctl enable nginx
dd if=/dev/zero of=/mnt/2GB.swap bs=1M count=2048 && chmod 600 /mnt/2GB.swap && mkswap /mnt/2GB.swap && swapon /mnt/2GB.swap
echo "/mnt/2GB.swap  swap   swap   defaults   0 0" >> /etc/fstab
mkdir -p /var/www
cd /var/www
git clone https://pininaes2@bitbucket.org/pininaes2/elenas.git
sed -i "s/localhost/${BACKEND-PUBLIC-IP}/g" /var/www/elenas/frontend_test/src/App.js
cd /var/www/elenas/frontend_test
npm install
yarn build
sed -i "s,root /var/www/html,root /var/www/elenas/frontend_test/build,g" /etc/nginx/sites-enabled/default
systemctl restart nginx

