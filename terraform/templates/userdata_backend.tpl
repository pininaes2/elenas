#!/bin/bash
apt update
apt dist-upgrade -y
apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose
systemctl enable docker
systemctl start docker
dd if=/dev/zero of=/mnt/2GB.swap bs=1M count=2048 && chmod 600 /mnt/2GB.swap && mkswap /mnt/2GB.swap && swapon /mnt/2GB.swap
echo "/mnt/2GB.swap  swap   swap   defaults   0 0" >> /etc/fstab
mkdir -p /var/www
cd /var/www
git clone https://pininaes2@bitbucket.org/pininaes2/elenas.git
cd /var/www/elenas/backend_test
docker login -u pininaes2 -p pininaes2
docker-compose up -d




