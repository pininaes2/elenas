variable "region" {
  type        = string
  description = "AWS region location"
}

variable "ec2_www_instance_type" {
  type        = string
  description = "Instance type EC2"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID Account"
}