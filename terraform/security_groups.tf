# Security group EC2 WWW
resource "aws_security_group" "ec2_frontend" {
  name        = "ec2-frontend"
  description = "ec2-frontend"
  vpc_id      = var.vpc_id
  tags        = local.frontend
}

resource "aws_security_group_rule" "ec2_frontend_ingress_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_frontend.id
  description       = "Allow ingress SSH protocol"
}

resource "aws_security_group_rule" "ec2_frontend_ingress_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_frontend.id
  description       = "Allow ingress HTTP protocol"
}

resource "aws_security_group_rule" "ec2_frontend_ingress_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_frontend.id
  description       = "Allow ingress HTTPS protocol"
}

resource "aws_security_group_rule" "ec2_frontend_egress_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_frontend.id
  description       = "Allow egress all trafic"
}

resource "aws_security_group" "ec2_backend" {
  name        = "ec2-backend"
  description = "ec2-backend"
  vpc_id      = var.vpc_id
  tags        = local.backend
}

resource "aws_security_group_rule" "ec2_backend_ingress_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_backend.id
  description       = "Allow ingress SSH protocol"
}

resource "aws_security_group_rule" "ec2_backend_ingress_docker" {
  type              = "ingress"
  from_port         = 4000
  to_port           = 4000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_backend.id
  description       = "Allow ingress docker frontend"
}

resource "aws_security_group_rule" "ec2_backend_egress_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_backend.id
  description       = "Allow egress all trafic"
}