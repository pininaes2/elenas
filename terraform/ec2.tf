resource "aws_instance" "ec2_backend" {
  ami                     = data.aws_ami.ubuntu.id
  instance_type           = var.ec2_www_instance_type
  availability_zone       = "${var.region}a"
  disable_api_termination = false
  key_name                = "private"
  monitoring              = "true"
  subnet_id               = sort(data.aws_subnet_ids.subnet.ids)[0]
  vpc_security_group_ids  = [aws_security_group.ec2_backend.id]
  user_data               = data.template_file.userdata_backend.rendered

  root_block_device {
    delete_on_termination = true
    volume_size           = 30
  }

  volume_tags = local.backend

  tags = local.backend
}

resource "aws_instance" "ec2_frontend" {
  ami                     = data.aws_ami.ubuntu.id
  instance_type           = var.ec2_www_instance_type
  availability_zone       = "${var.region}a"
  disable_api_termination = false
  key_name                = "private"
  monitoring              = "true"
  subnet_id               = sort(data.aws_subnet_ids.subnet.ids)[0]
  vpc_security_group_ids  = [aws_security_group.ec2_frontend.id]
  user_data               = data.template_file.userdata_frontend.rendered

  root_block_device {
    delete_on_termination = true
    volume_size           = 30
  }

  volume_tags = local.frontend

  tags = local.frontend
}
