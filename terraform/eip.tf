resource "aws_eip" "eip_ec2_frondend" {
  instance = aws_instance.ec2_frontend.id
  vpc      = true
  tags     = local.frontend
}
