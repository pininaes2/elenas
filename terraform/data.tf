data "aws_subnet_ids" "subnet" {
  vpc_id = var.vpc_id
}

data "aws_ami" "ubuntu" {

  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

data "template_file" "userdata_frontend" {
  template = file("${path.module}/templates/userdata_frontend.tpl")
  vars = {
    BACKEND-PUBLIC-IP = aws_instance.ec2_backend.public_ip
  }

  depends_on = [
    aws_instance.ec2_backend
  ]

}

data "template_file" "userdata_backend" {
  template = file("${path.module}/templates/userdata_backend.tpl")
  //vars = {}
}
